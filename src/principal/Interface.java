package principal;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JSlider;

public class Interface extends JFrame {

	private JPanel contentPane;
	private static JTextPane textPane;
	private static JTextField txtHoraInicio;
	private static JTextField txtPaquetesPerdidos;
	private static JTextField txtPaquetesEnviados;
	private JTextField txtDireccion;
	private static JCheckBox cbSonido;
	private Principal principal;
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
	public static JLabel backgroundImageLabel;
	public static JScrollBar sb;
	static DefaultCaret caret;
	static JCheckBox cbAutoScroll;
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Interface() {
//		backgroundImageLabel = new JLabel (new ImageIcon(".\\images\\Scyther.jpg"));
//		backgroundImageLabel.setBounds(0,0,700,700);
//		getContentPane().add(backgroundImageLabel);
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 470, 406);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 80, 428, 187);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scrollPane);
		sb = scrollPane.getVerticalScrollBar();
		scrollPane.setVerticalScrollBar(sb);
		
		
		setTextPane(new JTextPane());
		scrollPane.setViewportView(getTextPane());
		textPane.setEditable(false);
		caret = (DefaultCaret)textPane.getCaret();
		
		JLabel horaInicio = new JLabel("Hora inicio:");
		horaInicio.setBounds(10, 11, 85, 14);
		contentPane.add(horaInicio);
		
		txtHoraInicio = new JTextField();
		txtHoraInicio.setBounds(81, 8, 140, 20);
		txtHoraInicio.setEditable(false);
		contentPane.add(txtHoraInicio);
		txtHoraInicio.setColumns(10);
		
		JLabel lblPaquetesPerdidos = new JLabel("Paquetes perdidos:");
		lblPaquetesPerdidos.setBounds(10, 292, 113, 14);
		contentPane.add(lblPaquetesPerdidos);
		
		JLabel lblPaquetesEnviados = new JLabel("Paquetes enviados:");
		lblPaquetesEnviados.setBounds(10, 324, 113, 14);
		contentPane.add(lblPaquetesEnviados);
		
		txtPaquetesPerdidos = new JTextField();
		txtPaquetesPerdidos.setBounds(135, 289, 86, 20);
		txtPaquetesPerdidos.setEditable(false);
		contentPane.add(txtPaquetesPerdidos);
		txtPaquetesPerdidos.setColumns(10);
		
		txtPaquetesEnviados = new JTextField();
		txtPaquetesEnviados.setBounds(135, 321, 86, 20);
		txtPaquetesEnviados.setEditable(false);
		contentPane.add(txtPaquetesEnviados);
		txtPaquetesEnviados.setColumns(10);
		
		cbSonido = new JCheckBox("Sonido");
		cbSonido.setBackground(Color.GRAY);
		cbSonido.setBounds(242, 288, 85, 23);
		cbSonido.setSelected(true);
		contentPane.add(cbSonido);
		
		JLabel lblDireccion = new JLabel("Direccion:");
		lblDireccion.setBounds(10, 52, 60, 14);
		contentPane.add(lblDireccion);
		
		txtDireccion = new JTextField();
		txtDireccion.setBounds(81, 49, 140, 20);
		txtDireccion.setText("google.com");
		contentPane.add(txtDireccion);
		txtDireccion.setColumns(10);
		
		final JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.setBounds(229, 46, 89, 23);
		btnIniciar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				principal = new Principal();
				principal.setIp(txtDireccion.getText());
				principal.start();
				btnIniciar.setEnabled(false);
			}
		});
		contentPane.add(btnIniciar);
		
		JButton btnReiniciar = new JButton("Refresh");
		btnReiniciar.setBounds(242, 320, 89, 23);
		btnReiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//principal.stop();
				
				principal.hora=dtf.format(LocalDateTime.now());
				Interface.getTxtHoraInicio().setText(principal.hora.substring(10));
				principal.paquetesPerdidos=0;
				Interface.getTxtPaquetesPerdidos().setText("0");
				principal.paquetesEnviados=0;
				Interface.getTxtPaquetesEnviados().setText("0");
				Interface.getTextPane().setText("");
			}
		});
		contentPane.add(btnReiniciar);
		
		JButton btnDetener = new JButton("Detener");
		btnDetener.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("A�n no detengo");
			}
		});
		btnDetener.setBounds(343, 320, 89, 23);
		contentPane.add(btnDetener);
		
		cbAutoScroll = new JCheckBox("AutoScroll");
		cbAutoScroll.setSelected(true);
		cbAutoScroll.setBackground(Color.GRAY);
		cbAutoScroll.setBounds(326, 48, 85, 23);
		contentPane.add(cbAutoScroll);
		
		JSlider slider = new JSlider();
		slider.setBackground(Color.GRAY);
		slider.setBounds(343, 290, 89, 16);
		contentPane.add(slider);
		
	}
	public static void autoScroll(boolean b) {
		if(b) {
			caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		}else {
			caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
		}
		
	}

	public static JTextField getTxtHoraInicio() {
		return txtHoraInicio;
	}

	public void setTxtHoraInicio(JTextField txtHoraInicio) {
		this.txtHoraInicio = txtHoraInicio;
	}

	public static JTextField getTxtPaquetesPerdidos() {
		return txtPaquetesPerdidos;
	}

	public static JTextField getTxtPaquetesEnviados() {
		return txtPaquetesEnviados;
	}

	public JTextField getTxtDireccion() {
		return txtDireccion;
	}

	public void setTxtDireccion(JTextField txtDireccion) {
		this.txtDireccion = txtDireccion;
	}

	public static JTextPane getTextPane() {
		return textPane;
	}

	public void setTextPane(JTextPane textPane) {
		this.textPane = textPane;
	}
	
	public static JCheckBox getCbSonido() {
		return cbSonido;
	}
}
