package principal;
import java.awt.Color;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument; 
public class Principal extends Thread{
	
		private String ip = "google.com";
		private String respuesta;
		String hora;
		Integer paquetesPerdidos=0;
		Integer paquetesEnviados=-2;
	
		public void runSystemCommand(String command, String ip) {
		 
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		this.hora=dtf.format(LocalDateTime.now());
		Interface.getTxtHoraInicio().setText(this.hora.substring(10));
		Interface.getTxtPaquetesPerdidos().setText(this.paquetesPerdidos.toString());
		Interface.getTxtPaquetesEnviados().setText(this.paquetesEnviados.toString());
			try {
				Process p = Runtime.getRuntime().exec(command + ip);
				BufferedReader inputStream = new BufferedReader(
				new InputStreamReader(p.getInputStream()));

				String s = "";
				while ((s = inputStream.readLine()) != null) {
					if(!s.equals("")) {
						this.respuesta=recortaString(s);
						this.hora=dtf.format(LocalDateTime.now());
						if(Interface.getTextPane().getText().equals("")) {
							Interface.getTextPane().setText("-".repeat(100));
						}
						Interface.getTextPane().setText(Interface.getTextPane().getText() + "\n" + hora +"        "+ respuesta);
						Interface.autoScroll(Interface.cbAutoScroll.isSelected());
					}
					if(s.contains("agotado") || s.contains("error")) {
						this.paquetesPerdidos++;
						if(Interface.getCbSonido().isSelected()) {
							playSound(".\\sound\\Rupee.wav");
						}
						Interface.getTxtPaquetesPerdidos().setText(this.paquetesPerdidos.toString());
					}else {
						this.paquetesEnviados++;
						Interface.getTxtPaquetesEnviados().setText(this.paquetesEnviados.toString());
					}
					
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			
		public void run() {
			runSystemCommand("ping -t " , ip);	
		}
		
		public String recortaString(String s) {
			if(s.contains("tiempo=")) {
				return s.substring(s.indexOf("tiempo="),s.indexOf("TTL"));
			}
			
			if(s.contains("Haciendo")) {
				return "Ping a "+this.ip+" iniciado";
			}
			return s;
		}

		
		public void setIp(String ip) {
			this.ip = ip;
		}
		
		public static void playSound(String filePath) {
			File musicPath = new File(filePath);
			
			try {
				if(musicPath.exists()) {
					AudioInputStream audioInput = AudioSystem.getAudioInputStream(musicPath);
					Clip clip = AudioSystem.getClip();
					clip.open(audioInput);
					FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
					gainControl.setValue(-20.0f);
					//100 - lo que diga el slider, + agregar .0f como String concatenation
					clip.start();
					
				}else {
					System.out.println("ouch");
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}	
		}

		
		

}
